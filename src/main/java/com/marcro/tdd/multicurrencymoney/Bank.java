package com.marcro.tdd.multicurrencymoney;

import java.util.Hashtable;

public class Bank {
    private final Hashtable<Pair, Float> rates = new Hashtable<>();

    float rate(Currency from, Currency to) {
        if (from.equals(to)) {
            return 1;
        }
        return rates.get(new Pair(from, to));
    }

    void addRate(Currency from, Currency to, float rate) {
        rates.put(new Pair(from, to), rate);
    }

    private record Pair(Currency from, Currency to) {

        @Override
        public boolean equals(Object o) {
            Pair pair = (Pair) o;
            return from.equals(pair.from) && to.equals(pair.to);
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }
}
