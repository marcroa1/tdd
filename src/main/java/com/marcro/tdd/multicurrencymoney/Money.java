package com.marcro.tdd.multicurrencymoney;

class Money implements Expression {
    protected float amount;
    protected Currency currency;

    Money(float amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    static Money dollar(float amount) {
        return new Money(amount, Currency.USD);
    }
    static Money franc(float amount) {
        return new Money(amount, Currency.CHF);
    }



    @Override
    public Money reduce(Bank bank, Currency to) {
        float rate = bank.rate(currency, to);
        return new Money(amount / rate, to);
    }

    @Override
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }

    @Override
    public Expression times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }

    @Override
    public Expression divide(float divider) {
        return new Money(amount / divider, currency);
    }


    Currency currency() {
        return currency;
    }

    @Override
    public boolean equals(Object obj) {
        Money money = (Money) obj;
        return this.amount == money.amount
                && this.currency.equals(money.currency);
    }

    @Override
    public String toString() {
        return amount + " " + currency;
    }
}
