package com.marcro.tdd.multicurrencymoney;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * <h3>General Test List</h3>
 *
 * <li>Money rounding?</li>
 * <li>hashCode()</li>
 * <li>Equal null</li>
 * <li>Equal Object</li>
 */
public class MultiCurrencyMoneyTest {
    Bank bank;
    @Before
    public void setUp() {
        bank = new Bank();
        bank.addRate(Currency.CHF, Currency.USD, 2);
        bank.addRate(Currency.USD, Currency.CHF, 0.5f);

    }

    /**
     * <h3>Multiplication Test List</h3>
     *
     * <li style="text-decoration:line-through">$5 * 2 = $10</li>
     * <li style="text-decoration:line-through">5 CHF * 2 = 10 CHF</li>
     * <li style="text-decoration:line-through">Make "amount" private</li>
     * <li style="text-decoration:line-through">Dollar side effects?</li>
     * <li style="text-decoration:line-through">equals()</li>
     * <li style="text-decoration:line-through">Dollar-Franc duplication</li>
     * <li style="text-decoration:line-through">Common equals</li>
     * <li style="text-decoration:line-through">Common times</li>
     * <li style="text-decoration:line-through">Compare Francs with Dollars</li>
     * <li style="text-decoration:line-through">Currency?</li>
     * <li style="text-decoration:line-through">Delete testFrancMultiplication?</li>
     */
    @Test
    public void testMultiplication() {
        Money five = Money.dollar(5);
        assertEquals(Money.dollar(10), five.times(2));
        assertEquals(Money.dollar(15), five.times(3));
    }

    @Test
    public void testEquality() {
        assertEquals(Money.dollar(5), Money.dollar(5));
        assertNotEquals(Money.dollar(5), Money.dollar(6));
        assertNotEquals(Money.franc(5), Money.dollar(5));
    }

    @Test
    public void testCurrency() {
        assertEquals(Currency.USD, Money.dollar(1).currency());
        assertEquals(Currency.CHF, Money.franc(1).currency());
    }

    /**
     * <h3>Addition test list</h3>
     *
     * <li style="text-decoration:line-through">$5 + 10 CHF = $10 if rate is 2:1</li>
     * <li style="text-decoration:line-through">$5 + $5 = $10</li>
     * <li>Return Money from $5 + $5</li>
     * <li style="text-decoration:line-through">Bank.reduce(Money)</li>
     * <li style="text-decoration:line-through">Reduce Money with conversion</li>
     * <li style="text-decoration:line-through">Reduce(Bank, String)</li>
     * <li style="text-decoration:line-through">>Sum.plus</li>
     * <li style="text-decoration:line-through">>Expression.times</li>
     */

    @Test
    public void testSimpleAddition() {
        Money five = Money.dollar(5);
        Expression sum = five.plus(five);
        Money result = sum.reduce(bank, Currency.USD);
        assertEquals(Money.dollar(10), result);
    }

    @Test
    public void testPlusReturnsSum() {
        Money five = Money.dollar(5);
        Expression result = five.plus(five);
        Sum sum = (Sum) result;
        assertEquals(five, sum.augend);
        assertEquals(five, sum.addend);
    }

    @Test
    public void testReduceSum() {
        Expression sum = new Sum(Money.dollar(3), Money.dollar(4));
        Money result = sum.reduce(bank, Currency.USD);
        assertEquals(Money.dollar(7), result);
    }

    @Test
    public void testReduceMoney() {
        Money result = Money.dollar(1).reduce(bank, Currency.USD);
        assertEquals(Money.dollar(1), result);
    }

    @Test
    public void testReduceMoneyDifferentCurrency() {
        Money result = Money.franc(2).reduce(bank, Currency.USD);
        assertEquals(Money.dollar(1), result);

    }

    @Test
    public void testIdentityRate() {
        assertEquals(1f, bank.rate(Currency.USD, Currency.USD), 0f);
    }

    @Test
    public void testMixedAddition() {
        Expression fiveBuck = Money.dollar(5);
        Expression tenFrancs = Money.franc(10);
        Money result = fiveBuck.plus(tenFrancs).reduce(bank, Currency.USD);
        assertEquals(Money.dollar(10), result);
    }

    @Test
    public void testSumPlusMoney() {
        Expression fiveBuck = Money.dollar(5);
        Expression tenFrancs = Money.franc(10);
        Expression sum = new Sum(fiveBuck, tenFrancs).plus(fiveBuck);
        Money result = sum.reduce(bank, Currency.USD);
        assertEquals(Money.dollar(15), result);
    }

    @Test
    public void testSumTimes() {
        Expression fiveBucks = Money.dollar(5);
        Expression tenFrancs = Money.franc(10);
        Expression sum = new Sum(fiveBucks, tenFrancs).times(2);
        Money result = sum.reduce(bank, Currency.USD);
        assertEquals(Money.dollar(20), result);
    }

    @Test
    public void testComplexCalculation() {
        /*
        * (10 USD + 5 USD) * 2 = 30 USD
        * (20 CHF + 10 CHF) * 2 = 60 CHF
        * reduce(USD) -> 60 USD
        * plus(100 USD) -> 160 USD
        * reduce(CHF) -> 320CHF
        * */
        Money result = Money.dollar(10)
                .plus(Money.franc(20))
                .plus(Money.dollar(5))
                .plus(Money.franc(10))
                .times(2)
                .reduce(bank, Currency.USD)
                .plus(Money.dollar(100))
                .reduce(bank, Currency.CHF);
        assertEquals(Money.franc(320), result);
    }

    @Test
    public void testDivideExpressions() {
        Expression result = Money.dollar(20).divide(2f);
        assertEquals(Money.dollar(10), result);
    }

    @Test
    public void testDivideSumExpression() {
        Expression sum = new Sum(Money.dollar(10), Money.franc(10));
        Money result = sum.divide(2f).reduce(bank, Currency.USD);
        assertEquals(Money.dollar(7.5f), result);
    }
}
